package service.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import service.adapter.client.Client;

@Configuration
public class SOAPConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("service.adapter.wsdl");
        return marshaller;
    }

    @Bean
    public Client calculatorSOAPClient(Jaxb2Marshaller marshaller) {
        Client client = new Client();
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
